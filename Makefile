export PATH := $(HOME)/.local/bin:$(PATH)

PLAY=ansible-playbook --ask-become-pass
CHECK=ansible-playbook --ask-become-pass --check --diff
INIT_PLAYBOOK=init.yml
MAIN_PLAYBOOK=main.yml

PYTHON_VENV=.venv
ANSIBLE_COLLECTIONS_DIR=.ansible

.DEFAULT_GOAL := all
.PHONY: all
all: prepare lint

.PHONY: prepare
prepare:
	pip install --user -r requirements.txt
	test -d $(PYTHON_VENV) || python3 -m venv $(PYTHON_VENV)
	. $(PYTHON_VENV)/bin/activate && ( \
		pip install -r requirements.txt; \
		ansible-galaxy collection install --force-with-deps -r requirements.yml; \
	)

.PHONY: clean
clean:
	rm -rf $(ANSIBLE_COLLECTIONS_DIR)
	rm -rf $(PYTHON_VENV)

.PHONY: lint
lint:
	. $(PYTHON_VENV)/bin/activate && ( \
		ansible-playbook $(INIT_PLAYBOOK) --syntax-check; \
		ansible-playbook $(MAIN_PLAYBOOK) --syntax-check; \
		ansible-lint -c .config/ansible-lint.yml *.yml; \
	)

.PHONY: check
check: lint
	$(CHECK) $(MAIN_PLAYBOOK)

.PHONY: play_init
play_init:
	$(PLAY) $(INIT_PLAYBOOK)

.PHONY: play
play:
	$(PLAY) $(MAIN_PLAYBOOK)
